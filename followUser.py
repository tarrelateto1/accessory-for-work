
from InstagramAPI import InstagramAPI
import argparse

from time import sleep
import time

my_parser = argparse.ArgumentParser()
my_parser.add_argument('--id',action='store',type=str,required=True)
my_parser.add_argument('--password',action='store',type=str,required=True)
my_parser.add_argument('--follow',action='store',type=str,required=True)


args = my_parser.parse_args()
# config
_id = args.id
_password = args.password
_followUser = args.follow  #margie_rasri
# Global variable
total_follow = 0
# function definition
def get_likes_list(username):
    api.login()
    api.searchUsername(username)
    result = api.LastJson
    username_id = result['user']['pk'] # Get user ID
    user_posts = api.getUserFeed(username_id) # Get user feed
    result = api.LastJson
    media_id = result['items'][0]['id'] # Get most recent post
    api.getMediaLikers(media_id) # Get users who liked
    users = api.LastJson['users']
    for user in users: # Push users to list
        users_list.append({'pk':user['pk'], 'username':user['username']})

def follow_users(users_list):
    global total_follow
    api.login()
    api.getSelfUsersFollowing() # Get users which you are following
    result = api.LastJson
    for user in result['users']:
        following_users.append(user['pk'])
    for user in users_list:
        
        if not user['pk'] in following_users: # if new user is not in your following users     
            total_follow += 1              
            print(str(total_follow)+'.Following @' + user['username'] + ' At '+time.ctime(time.time()))
            response = api.follow(user['pk'])
            if total_follow == 200 :
                total_follow = 0
                sleep(60*60*16)
            if response == False :
                total_follow = 0
                sleep(1*60*60*24)
                return
            # after first test set this really long to avoid from suspension
            sleep(120)
        else:
            print('Already following @' + user['username'] + ' At ' +time.ctime(time.time()))
            sleep(1)
# varaible definition
while True:
    following_users = []
    users_list = []
    # Login
    print('------ '+ _id + ' ------')
    api = InstagramAPI(_id, _password)
    api.login()


    get_likes_list(_followUser)
    follow_users(users_list)