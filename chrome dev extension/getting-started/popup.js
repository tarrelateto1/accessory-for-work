// Initialize butotn with users's prefered color
let KDPPrice_50Pages = document.getElementById("KDPPrice_50Pages");
let KDPPrice_100Pages = document.getElementById("KDPPrice_100Pages");
let KDPPrice_120Pages = document.getElementById("KDPPrice_120Pages");
let KDPPrice_150Pages = document.getElementById("KDPPrice_150Pages");
let KDPPrice_200Pages = document.getElementById("KDPPrice_200Pages");


// When the button is clicked, inject setPageBackgroundColor into current page
KDPPrice_50Pages.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: KDPPrice_50Pages_action,
  });
});

KDPPrice_100Pages.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: KDPPrice_100Pages_action,
  });
});

KDPPrice_120Pages.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: KDPPrice_120Pages_action,
  });
});

KDPPrice_150Pages.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: KDPPrice_150Pages_action,
  });
});

KDPPrice_200Pages.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: KDPPrice_200Pages_action,
  });
});



// The body of this function will be execuetd as a content script inside the
// current page
function KDPPrice_50Pages_action() {
  document.querySelector("#data-pricing-print-us-price-input > input").value = "4.99";
  
  document.querySelector("#data-pricing-print-uk-price-input > input").value = "3.99";

  document.querySelector("#data-pricing-print-de-price-input > input").value = "4.49";

  document.querySelector("#data-pricing-print-fr-price-input > input").value = "4.49";

  document.querySelector("#data-pricing-print-es-price-input > input").value = "4.49";

  document.querySelector("#data-pricing-print-it-price-input > input").value = "4.49";

  document.querySelector("#data-pricing-print-jp-price-input > input").value = "699";

  document.querySelector("#data-pricing-print-ca-price-input > input").value = "5.99";

  document.querySelector("#data-pricing-print-au-price-input > input").value = "7.49";

  document.querySelector("#data-pricing-print-nl-price-input > input").value = "4.49";
}

function KDPPrice_100Pages_action() {
  document.querySelector("#data-pricing-print-us-price-input > input").value = "5.99";
  
  document.querySelector("#data-pricing-print-uk-price-input > input").value = "4.49";

  document.querySelector("#data-pricing-print-de-price-input > input").value = "5.49";

  document.querySelector("#data-pricing-print-fr-price-input > input").value = "5.49";

  document.querySelector("#data-pricing-print-es-price-input > input").value = "5.49";

  document.querySelector("#data-pricing-print-it-price-input > input").value = "5.49";

  document.querySelector("#data-pricing-print-jp-price-input > input").value = "749";

  document.querySelector("#data-pricing-print-ca-price-input > input").value = "7.49";

  document.querySelector("#data-pricing-print-au-price-input > input").value = "7.99";

  document.querySelector("#data-pricing-print-nl-price-input > input").value = "5.49";
}
function KDPPrice_120Pages_action() {
  document.querySelector("#data-pricing-print-us-price-input > input").value = "6.99";
  
      document.querySelector("#data-pricing-print-uk-price-input > input").value = "5.49";
  
      document.querySelector("#data-pricing-print-de-price-input > input").value = "6.49";
  
      document.querySelector("#data-pricing-print-fr-price-input > input").value = "6.49";
  
      document.querySelector("#data-pricing-print-es-price-input > input").value = "6.49";
  
      document.querySelector("#data-pricing-print-it-price-input > input").value = "6.49";
  
      document.querySelector("#data-pricing-print-jp-price-input > input").value = "899";
  
      document.querySelector("#data-pricing-print-ca-price-input > input").value = "8.99";
  
      document.querySelector("#data-pricing-print-au-price-input > input").value = "9.49";
  
      document.querySelector("#data-pricing-print-nl-price-input > input").value = "6.49";
}
function KDPPrice_150Pages_action() {
  document.querySelector("#data-pricing-print-us-price-input > input").value = "7.59";
  
      document.querySelector("#data-pricing-print-uk-price-input > input").value = "5.99";
  
      document.querySelector("#data-pricing-print-de-price-input > input").value = "6.97";
  
      document.querySelector("#data-pricing-print-fr-price-input > input").value = "4.49";
  
      document.querySelector("#data-pricing-print-es-price-input > input").value = "6.97";
  
      document.querySelector("#data-pricing-print-it-price-input > input").value = "6.97";
  
      document.querySelector("#data-pricing-print-jp-price-input > input").value = "997";
  
      document.querySelector("#data-pricing-print-ca-price-input > input").value = "9.47";
  
      document.querySelector("#data-pricing-print-au-price-input > input").value = "9.97";
  
      document.querySelector("#data-pricing-print-nl-price-input > input").value = "6.97";
}
function KDPPrice_200Pages_action() {
  document.querySelector("#data-pricing-print-us-price-input > input").value = "7.99";
  
  document.querySelector("#data-pricing-print-uk-price-input > input").value = "5.99";

  document.querySelector("#data-pricing-print-de-price-input > input").value = "7.99";

  document.querySelector("#data-pricing-print-fr-price-input > input").value = "7.99";

  document.querySelector("#data-pricing-print-es-price-input > input").value = "7.99";

  document.querySelector("#data-pricing-print-it-price-input > input").value = "7.99";

  document.querySelector("#data-pricing-print-jp-price-input > input").value = "999";

  document.querySelector("#data-pricing-print-ca-price-input > input").value = "9.99";

  document.querySelector("#data-pricing-print-au-price-input > input").value = "10.99";

  document.querySelector("#data-pricing-print-nl-price-input > input").value = "7.99";
}
