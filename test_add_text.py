import numpy as np
import cv2
from PIL import ImageFont, ImageDraw, Image  

# Open image, Attention: OpenCV uses BGR ordering by default!
image = cv2.imread('output.png', cv2.IMREAD_COLOR)

«

text = '9999.-'
# Set up text properties
loc = (550, 500)
c_fg = (0, 0, 255, 100)
c_bg = (0, 0, 0, 100)

# Initialize overlay text plane
overlay = np.zeros((image.shape[0], image.shape[1], 4), np.uint8)

# Put text outline, larger thickness, color of outline (here: black)
cv2.putText(overlay, text, loc, cv2.FONT_HERSHEY_COMPLEX, 3.0, c_bg, 9, cv2.LINE_AA)

# Blur text plane (including alpha channel): Heavy blur
overlay = cv2.GaussianBlur(overlay, (21, 21), sigmaX=10, sigmaY=10)

# Put text, normal thickness, color of overlay (here: yellow)
cv2.putText(overlay, text, loc, cv2.FONT_HERSHEY_COMPLEX, 3.0, c_fg, 9, cv2.LINE_AA)

# Blur text plane (inclusing alpha channel): Very slight blur
# overlay = cv2.GaussianBlur(overlay, (3, 3), sigmaX=0.5, sigmaY=0.5)

output = np.zeros(image.shape, np.uint8)
for i in np.arange(3):
    output[:, :, i] = image[:, :, i] * ((255 - overlay[:, :, 3]) / 255) + overlay[:, :, i] * (overlay[:, :, 3] / 255)


cv2.imshow('output', output)
cv2.waitKey(0)
cv2.destroyAllWindows()