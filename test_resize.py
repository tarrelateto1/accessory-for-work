import cv2 
  
# Save image in set directory 
# Read RGB image 
img = cv2.imread('IMG_3488.JPG')

width = 1080
height = 1080
dim = (width,height)

resized = cv2.resize(img,dim,interpolation = cv2.INTER_AREA)
  
print('Resized Dimensions : ',resized.shape)
# Output img with window name as 'image' 
cv2.imshow('image', resized)  
  
# Maintain output window utill 
# user presses a key 
cv2.waitKey(0)         
  
# Destroying present windows on screen 
cv2.destroyAllWindows()  