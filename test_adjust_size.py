import cv2
import os 
import argparse
import re
pattern = "[\"|\'].*\.(?i:jpg|gif|png|bmp)[\"|\']"

my_parser = argparse.ArgumentParser()
my_parser.add_argument('--dir',action='store',type=str,required=True)
args = my_parser.parse_args()
list_file=[]
dir=args.dir
for path in os.listdir(dir):
    full_path = os.path.join(dir, path)
    if os.path.isfile(full_path):
        list_file.append(full_path)
white = [255,255,255]

regex = re.compile(r'\w*\.(?:jpg|jpeg|gif|png)')
list_file = list(filter(regex.search, map(lambda x:x.lower(),list_file)))

length = len(list_file)

for i in range(length):
    file_path = list_file.pop()
    img1 = cv2.imread(file_path)
    width = 1080
    height = 1080
    dim = (width,height)
    constant = 0
    print(img1.shape)
    if img1.shape[0] > img1.shape[1]:
        boarder = int(abs(img1.shape[0]-img1.shape[1])/2)
        constant = cv2.copyMakeBorder(img1,0,0,boarder,boarder,cv2.BORDER_CONSTANT,value=white)

        

    if img1.shape[0] < img1.shape[1]:
        boarder = int(abs(img1.shape[0]-img1.shape[1])/2)
        constant = cv2.copyMakeBorder(img1,boarder,boarder,0,0,cv2.BORDER_CONSTANT,value=white)
    
    resized = cv2.resize(constant,dim,interpolation = cv2.INTER_AREA)


    cv2.imwrite(file_path,resized)


