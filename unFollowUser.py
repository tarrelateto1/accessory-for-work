from InstagramAPI import InstagramAPI
import argparse

from time import sleep
import time

my_parser = argparse.ArgumentParser()
my_parser.add_argument('--id',action='store',type=str,required=True)
my_parser.add_argument('--password',action='store',type=str,required=True)
# my_parser.add_argument('--follow',action='store',type=str,required=True)


args = my_parser.parse_args()
# config
_id = args.id
_password = args.password
# _followUser = args.follow  #margie_rasri
# Global variable

users_list = []
following_users = []
follower_users = []

def unfollow_users():
    api.login()
    api.getSelfUserFollowers()
    result = api.LastJson

    total_unfollow = 1
    for user in result['users']:
        follower_users.append({'pk':user['pk'], 'username':user['username']})

    api.getSelfUsersFollowing()
    result = api.LastJson
    for user in result['users']:
        following_users.append({'pk':user['pk'],'username':user['username']})
    for user in following_users:
        if not user['pk'] in follower_users and total_unfollow < 200:
            response = api.unfollow(user['pk'])
            print(str(total_unfollow)+'.Unfollowing @' + user['username']+ ' At '+time.ctime(time.time()))
            # set this really long to avoid from suspension
            total_unfollow += 1
            if response == False :
                total_unfollow = 0
                sleep(1*60*60*24)
                return
            sleep(60*2)
        elif total_unfollow == 200:
            total_unfollow = 0
            sleep(60*60*16)
            return

while True:
    print('------ '+ _id + ' ------')
    api = InstagramAPI(_id, _password)
    api.login()
    unfollow_users()
