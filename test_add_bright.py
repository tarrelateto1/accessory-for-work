import cv2

image = cv2.imread('output.png')

alpha = 0.9 # contrast 1.0-3.0
beta = 10# brightness 0-100

adjusted = cv2.convertScaleAbs(image,alpha = alpha , beta= beta)
cv2.imshow('adjusted',adjusted)
cv2.imshow('ori',image)
cv2.waitKey(0)