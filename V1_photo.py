import cv2
import os 
import argparse
import re
from PIL import Image, ImageDraw, ImageFilter

white = [255,255,255]
black = [0,0,0]
alpha = 0.9 # contrast 1.0-3.0
beta = 10# brightness 0-100
white = [255,255,255]
black = [0,0,0]
pattern = "[\"|\'].*\.(?i:jpg|gif|png|bmp)[\"|\']"

my_parser = argparse.ArgumentParser()
my_parser.add_argument('--dir',action='store',type=str,required=True)
args = my_parser.parse_args()
list_file_v1=[]
list_file_v2=[]
list_file_v3=[]
dir_v1=args.dir+'/v1'
dir_v2=args.dir+'/v2'
dir_v3=args.dir+'/v3'

for path in os.listdir(dir_v1):
    full_path = os.path.join(dir_v1, path)
    if os.path.isfile(full_path):
        list_file_v1.append(full_path)

for path in os.listdir(dir_v2):
    full_path = os.path.join(dir_v2, path)
    if os.path.isfile(full_path):
        list_file_v2.append(full_path)

for path in os.listdir(dir_v3):
    full_path = os.path.join(dir_v3, path)
    if os.path.isfile(full_path):
        list_file_v3.append(full_path)

regex = re.compile(r'\w*\.(?:jpg|jpeg|gif|png)')
list_file_v1 = list(filter(regex.search, map(lambda x:x.lower(),list_file_v1)))
list_file_v2 = list(filter(regex.search, map(lambda x:x.lower(),list_file_v2)))
list_file_v3 = list(filter(regex.search, map(lambda x:x.lower(),list_file_v3)))
print(list_file_v3)

# version 1
for file_path in list_file_v1:
    img1 = cv2.imread(file_path)
    boarder_white = int(10)
    boarder_black = int(5)
    constant = cv2.copyMakeBorder(img1,boarder_white,boarder_white,boarder_white,boarder_white,cv2.BORDER_CONSTANT,value=white)
    cv2.imwrite(file_path,constant)
    img1 = cv2.imread(file_path)
    constant = cv2.copyMakeBorder(img1,boarder_black,boarder_black,boarder_black,boarder_black,cv2.BORDER_CONSTANT,value=black)
    cv2.imwrite(file_path,constant)
    img1 = cv2.imread(file_path)
    constant = cv2.copyMakeBorder(img1,boarder_white,boarder_white,boarder_white,boarder_white,cv2.BORDER_CONSTANT,value=white)
    cv2.imwrite(file_path,constant)
    
    
# version 2 
for file_path in list_file_v2:
    img1 = cv2.imread(file_path)
    width = 1080
    height = 1080
    dim = (width,height)
    constant = 0
    if img1.shape[0] > img1.shape[1]:
        boarder = int(abs(img1.shape[0]-img1.shape[1])/2)
        constant = cv2.copyMakeBorder(img1,0,0,boarder,boarder,cv2.BORDER_CONSTANT,value=white)
        resized = cv2.resize(constant,dim,interpolation = cv2.INTER_AREA)
        adjusted = cv2.convertScaleAbs(resized,alpha = alpha , beta= beta)
        cv2.imwrite(file_path,adjusted)
    if img1.shape[0] < img1.shape[1]:
        boarder = int(abs(img1.shape[0]-img1.shape[1])/2)
        constant = cv2.copyMakeBorder(img1,boarder,boarder,0,0,cv2.BORDER_CONSTANT,value=white)
        resized = cv2.resize(constant,dim,interpolation = cv2.INTER_AREA)
        adjusted = cv2.convertScaleAbs(resized,alpha = alpha , beta= beta)
        cv2.imwrite(file_path,adjusted)

# version 3
for file_path in list_file_v3:
    img1 = cv2.imread(file_path)
    width = 1080
    height = 1080
    dim = (width,height)
    constant = 0
    if img1.shape[0] > img1.shape[1]:
        boarder = int(abs(img1.shape[0]-img1.shape[1])/2)
        constant = cv2.copyMakeBorder(img1,0,0,boarder,boarder,cv2.BORDER_CONSTANT,value=white)
        resized = cv2.resize(constant,dim,interpolation = cv2.INTER_AREA)
        adjusted = cv2.convertScaleAbs(resized,alpha = alpha , beta= beta)
        cv2.imwrite(file_path,adjusted)
    if img1.shape[0] < img1.shape[1]:
        boarder = int(abs(img1.shape[0]-img1.shape[1])/2)
        constant = cv2.copyMakeBorder(img1,boarder,boarder,0,0,cv2.BORDER_CONSTANT,value=white)
        resized = cv2.resize(constant,dim,interpolation = cv2.INTER_AREA)
        adjusted = cv2.convertScaleAbs(resized,alpha = alpha , beta= beta)
        cv2.imwrite(file_path,adjusted)
    if img1.shape[0] == img1.shape[1]:
        resized = cv2.resize(img1,dim,interpolation = cv2.INTER_AREA)
        adjusted = cv2.convertScaleAbs(resized,alpha = alpha , beta= beta)
        cv2.imwrite(file_path,adjusted)
    # add image
    print(file_path)
    img1 = Image.open(file_path)
    img2 = Image.open('sales1.png')
    img1.paste(img2,(0,0),img2)
    img1.save(file_path,quality=95)
    # from V1
    img1 = cv2.imread(file_path)
    boarder_white = int(10)
    boarder_black = int(5)
    constant = cv2.copyMakeBorder(img1,boarder_white,boarder_white,boarder_white,boarder_white,cv2.BORDER_CONSTANT,value=white)
    cv2.imwrite(file_path,constant)
    img1 = cv2.imread(file_path)
    constant = cv2.copyMakeBorder(img1,boarder_black,boarder_black,boarder_black,boarder_black,cv2.BORDER_CONSTANT,value=black)
    cv2.imwrite(file_path,constant)
    img1 = cv2.imread(file_path)
    constant = cv2.copyMakeBorder(img1,boarder_white,boarder_white,boarder_white,boarder_white,cv2.BORDER_CONSTANT,value=white)
    cv2.imwrite(file_path,constant)



