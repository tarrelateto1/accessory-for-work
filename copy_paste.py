from pynput.keyboard import Key,Listener,Controller
import clipboard

keyboard = Controller()
queue = []

def on_press(key):
    global queue
    try:
        print('{0} pressed'.format(key))
        # if key.char == ('0'):
        #     listener.stop()
        # if key.char == ('1'):
        if key.char == ('0'):
            queue = []
    except:
        print('test')

def on_release(key):
    global queue
    global keyboard
    try:
        print('{0} released'.format(
            key))
        if key == Key.esc:
            listener.stop()
        if key == Key.ctrl:
            # Stop listener
            length = len(queue)
            for i in range(length):
                word_type = queue.pop()
                keyboard.type(word_type)
                keyboard.press(Key.enter)
        if key == Key.alt_l:
            text_clipboard = clipboard.paste()
            print(text_clipboard)
            queue = text_clipboard.split("\n")
            print(queue)

    except:
        print('something wrong')


with Listener( on_press=on_press,on_release=on_release) as listener:
    listener.join()

"""
Tutorial for use copy+paste
Key :   action
Esc : exit program
crtl_r : copy text from copyclipboard and save to queue
alt_l : paste all text from queue

"""